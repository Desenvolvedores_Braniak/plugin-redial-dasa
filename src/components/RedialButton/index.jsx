import React from "react";
import { Component } from "react";
import { Button, Actions } from "@twilio/flex-ui";

import styles from "./styles.module.css";

const TwilioVoiceChannel = "TCa4275e2112e971fe4a1abd9b31915066";
const TwilioRedialQueue = "WQ02b8d4c7e7ca006ceb5faed937ec5585";

export class RedialButton extends Component {
  constructor(props) {
    super(props);

    this._redialMethod = this._redialMethod.bind(this);
  }

  _redialMethod(sid, destination) {
    console.log(`[${sid}]: Selected phone to redial: ${destination}`);

    Actions.invokeAction("CompleteTask", { sid });

    console.log(
      `Starting an outbound call to ${destination} from queue ${TwilioRedialQueue}`
    );

    Actions.invokeAction("StartOutboundCall", {
      destination,
      queueSid: TwilioRedialQueue,
    });
  }

  render() {
    try {
      const { status, taskChannelSid, sid, attributes } = this.props?.task;

      const phone =
        attributes.direction === "outbound"
          ? attributes.outbound_to
          : attributes.from;

      return TwilioVoiceChannel == taskChannelSid && status == "wrapping" ? (
        <Button
          className={styles.redialButton}
          onClick={() => this._redialMethod(sid, phone)}
        >
          REDIAL
        </Button>
      ) : null;
    } catch (err) {
      console.error(err);
      return null;
    }
  }
}
